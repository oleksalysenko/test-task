describe('second script', function() {
  describe('compareDates()', function() {
    beforeEach(function() {
      sinon.spy(console, 'error');
    });

    afterEach(function() {
      console.error.restore();
    });

    it('should return 1 if the first date is greater than the second date', function() {
      let date1 = (new Date('2017 4 15')).getTime();
      let date2 = (new Date('2015 4 15')).getTime();
      expect(compareDates(date1, date2)).to.eql(1);
    });
    it('should return -1 if the first date is less than the second date', function() {
      let date1 = (new Date('2017 4 10')).getTime();
      let date2 = (new Date('2017 4 15')).getTime();
      expect(compareDates(date1, date2)).to.eql(-1);
    });
    it('should return 0 if two dates are equal', function() {
      let date1 = (new Date('2017 4 10')).getTime();
      let date2 = (new Date('2017 4 10')).getTime();
      expect(compareDates(date1, date2)).to.eql(0);
    });
    it('should print error message if the first passed argument isn\'t in epoch date in milliseconds format', function() {
      let invalidDate = 'abcde';
      let validDate = (new Date('2017 4 10')).getTime();
      compareDates(invalidDate, validDate);
      expect( console.error.calledWith('Illegal argument. Provided value should be a valid timestamp')).to.be.true;
    });
    it('should print error message if the second passed argument isn\'t in epoch date in milliseconds format', function() {
      let validDate = (new Date('2017 4 10')).getTime();
      let invalidDate = 'abcde';
      compareDates(validDate, invalidDate);
      expect( console.error.calledWith('Illegal argument. Provided value should be a valid timestamp')).to.be.true;
    });
  });

  describe('getDate()', function() {
    it('should return date in format "day month year"', function() {
      expect(getDate('2017 05 06')).to.eql('06 May 2017');
    });
  });
});
