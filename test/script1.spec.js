describe('first script', function() {
  describe('findTheLongestWords()', function() {
    it('should return an array', function() {
      expect(findTheLongestWords(['some','text','goes','here'])).to.be.an('array');
    });

    it('should return an array containing one item if one word with the longest length is provided', function() {
      let actual = findTheLongestWords(['Thomas', 'Cook', 'price']);
      expect(actual).to.eql(['Thomas']);
    });

    it('should return an array containing several items if more than one word with the longest length are provided', function() {
      let actual = findTheLongestWords(['holidays', 'flights', 'extra', 'Thomas', 'Cook', 'bookings']);
      expect(actual).to.eql(['holidays', 'bookings']);
    });

    it('should return empty array if provided array doesn\'t contain any elements', function() {
      let actual = findTheLongestWords([]);
      expect(actual).to.eql([]);
    });

    it('should return empty array if provided array contains only elements with empty string', function() {
      let actual = findTheLongestWords(['', '', ''])
      expect(actual).to.eql([]);
    });
  });

  describe('parsePageContent()', function() {
    it('should return an array', function() {
      expect(parsePageContent(document.implementation.createHTMLDocument('Test empty document'))).to.be.an('array');
    });

    it('should parse only body tag', function() {
      let doc = createTestDocument();

      let expected = ['Test', 'content', 'inside', 'body', 'tag'];

      expect(parsePageContent(doc)).to.eql(expected);
    });

    it('should skip unwanted text tags (style, script, noscript)', function() {
      let doc = createTestDocument();
      let script = doc.createElement('script');
      let style = doc.createElement('style');
      script.innerHTML = 'console.log("I will never be parsed");';
      style.innerHTML = 'body { background: white }';
      doc.body.appendChild(script);
      doc.body.appendChild(style);

      let expected = ['Test', 'content', 'inside', 'body', 'tag'];

      expect(parsePageContent(doc)).to.eql(expected);
    });

    it('should ignore all special symbols and digits', function() {
      let doc = createTestDocument();
      let span = doc.createElement('span');
      let div = doc.createElement('div');
      span.innerHTML = 'email123@test.mail';
      div.innerHTML = 'from $250';
      doc.body.appendChild(span);
      doc.body.appendChild(div);

      let expected = ['Test', 'content', 'inside', 'body', 'tag', 'email', 'test', 'mail', 'from'];

      expect(parsePageContent(doc)).to.eql(expected);
    });

    it('should not parse elements with no content', function() {
      let doc = createTestDocument();
      let p = doc.createElement('p');
      let div = doc.createElement('div');
      let span = doc.createElement('span');
      doc.body.appendChild(p);
      doc.body.appendChild(div);
      doc.body.appendChild(span);

      let expected = ['Test', 'content', 'inside', 'body', 'tag'];

      expect(parsePageContent(doc)).to.eql(expected);
    });
  });
});

function createTestDocument() {
  let doc = document.implementation.createHTMLDocument('Test document'); // creates document with title 'Test document'
  let p = doc.createElement('p');
  p.innerHTML = 'Test content inside body tag';
  doc.body.appendChild(p);

  return doc;
}
