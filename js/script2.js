// this snippet should be executed on the landing page
let date = getDate('2017, 5, 1');
setDepartureDate(date);

// this one on the search page
let departureDate = Date.parse(sessionStorage.getItem('departureDate'));
let searchPanelDate = Date.parse(document.getElementById('SearchbarReadonly-when').textContent);
let result = compareDates(departureDate, searchPanelDate)
  ? 'the date specified on the landing page is greater than the date shown on the search panel'
  : 'dates are equal'

console.log(result);

// Expected date format: 'year, month, day'. For example: '2017, 5, 24'
function getDate(date) {
  let d;

  if (date === undefined)
    d = new Date();
  else
    d = new Date(date);

  d = d.toDateString().split(' ');
  let day = d[2];
  let month = d[1];
  let year = d[3];
  return `${day} ${month} ${year}`;
}

function setDepartureDate(date) {
  let departureDate = document.getElementById('when').value = date;
  sessionStorage.setItem('departureDate', departureDate);
}

// Expected date format is epoch date in milliseconds
function compareDates(date1, date2) {
  if (!isValidTimestamp(date1) || !isValidTimestamp(date2)) {
    console.error('Illegal argument. Provided value should be a valid timestamp');
  } else {
    if (date1 > date2)
      return 1;
    else if (date1 < date2)
      return -1;
    else
      return 0;
  }
}

function isValidTimestamp(timestamp) {
  return (new Date(timestamp)).getTime() > 0;
}
