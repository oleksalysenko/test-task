function parsePageContent(document) {
  // filter, which is used to reject all special tags such as script, style, etc. Add more tags if needed
  let rejectSpecialTagsFilter = {
    acceptNode: function(node) {
      let tag = node.parentNode.nodeName;
      if (tag !== 'SCRIPT' && tag !== 'NOSCRIPT' && tag !== 'STYLE') {
        return NodeFilter.FILTER_ACCEPT;
      }
    }
  };

  let result = [];
  let walker = document.createTreeWalker(document.body, NodeFilter.SHOW_TEXT, rejectSpecialTagsFilter, false);
  let tag;
  while (tag = walker.nextNode()) {
    tag.nodeValue.replace(/[0-9`~!@#$№%^&*()–_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, ' ') // BOGUS hacky way to leave only 'words' from page content
      .split(/\s+/g)
      .filter(function(item) {
        return item.length > 0;
      })
      .forEach(function(item) {
        result.push(item);
      });
  }

  return result;
}

function findTheLongestWords(arr) {
  arr.sort(function(str1, str2) {
    return str1.length > str2.length ? -1 : str1.length < str2.length ? 1 : 0;
  });

  let longestWords = [];

  if (arr.length > 0) {
    let maxLength = arr[0].length;

    for (let i = 0; i < arr.length; i++) {
      if (arr[i].length > 0 && arr[i].length == maxLength) {
        longestWords.push(arr[i]);
      } else {
        break;
      }
    }
  }

  return longestWords;
}

function printTheLongestWords(arr) {
  if (arr.length == 0) {
    console.log('No words were found on the page')
  } else if (arr.length == 1) {
    console.log('The longest word is - ' + arr[0])
  } else {
    console.log(arr.length + ' words with the longest length were found');
    for (let i = 0; i < arr.length; i++) {
      console.log(arr[i]);
    }
  }
}

function getDocument() {
  return document;
}

let pageContent = parsePageContent(getDocument());
let result = findTheLongestWords(pageContent);
printTheLongestWords(result);
