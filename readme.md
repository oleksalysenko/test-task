# Test task 1

Implement a script to find the looooongest word on any page.

----
## usage
To find the longest word on any page

1. Go to any web page
2. Open the DevTools Console (e.g Chrome browser)
3. Copy the code snippet from *js/script1.js* and paste it to the console
4. Hit enter

----
## testing
Testing is implemented using **Mocha** and **Chai**.

To install testing tools go to the terminal, in the root folder run **npm install** or **yarn install**

To run tests go to the root folder and open *testrunner1.html* in your browser.


----
# Test task 2

Implement a script which sets a date on the [https://www.thomascook.com/] (https://www.thomascook.com/) page, then navigates to the search page where it compares the date value on the search panel with the one set on the landing page. Print result of the comparison to a browser console.

----
## usage
Since this task needs a cross page execution, we can't run it in the browser console.
The implemented code is in the *js/script2.js* file.

----
## testing
To run tests go to the root folder and open *testrunner2.html* in your browser.
